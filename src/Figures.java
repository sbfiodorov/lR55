class Circle implements Shape, Comparable<Shape> {
    private final double radius;
    private Color fillColor;
    private Color borderColor;

    Circle(double radius, Color fillColor, Color borderColor) {
        this.radius = radius;
        this.fillColor = fillColor;
        this.borderColor = borderColor;
        if (radius <= 0) {
            throw new IllegalArgumentException("Parameters can't be <= 0");
        }
    }

    @Override
    public double area() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public Color fillColor() {
        return fillColor;
    }

    @Override
    public Color borderColor() {
        return borderColor;
    }

    @Override
    public String toString() {
        return "Circle" +
                "{" +
                "radius=" + radius +
                ", fillColor=" + fillColor +
                ", borderColor=" + borderColor +
                '}';
    }

    @Override
    public int compareTo(Shape shape) {
        return this.fillColor.compareTo(shape.fillColor());
    }
}

class Square implements Shape, Comparable<Shape> {
    private final double side;
    private Color fillColor;
    private Color borderColor;

    Square(double side, Color fillColor, Color borderColor) {
        this.side = side;
        this.fillColor = fillColor;
        this.borderColor = borderColor;
        if (side <= 0) {
            throw new IllegalArgumentException("Parameters can't be <= 0");
        }
    }

    @Override
    public double area() {
        return Math.pow(side, 2);
    }

    @Override
    public Color fillColor() {
        return fillColor;
    }

    @Override
    public Color borderColor() {
        return borderColor;
    }

    @Override
    public String toString() {
        return "Square" +
                " { " +
                "side = " + side +
                ", fillColor = " + fillColor +
                ", borderColor = " + borderColor +
                " }";
    }

    @Override
    public int compareTo(Shape shape) {
        return this.fillColor.compareTo(shape.fillColor());
    }
}

class Rectangle implements Shape, Comparable<Shape> {
    private final double width;
    private final double height;
    private Color fillColor;
    private Color borderColor;

    Rectangle(double width, double height, Color fillColor, Color borderColor) {
        this.width = width;
        this.height = height;
        this.fillColor = fillColor;
        this.borderColor = borderColor;
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Parameters can't be <= 0");
        }
        if (width == height) {
            throw new IllegalArgumentException("Parameters can't be '==', it's a Rectangle");
        }
    }

    @Override
    public double area() {
        return width * height;
    }

    @Override
    public Color fillColor() {
        return fillColor;
    }

    @Override
    public Color borderColor() {
        return borderColor;
    }

    @Override
    public String toString() {
        return "Rectangle" +
                " { " +
                "width = " + width +
                ", height = " + height +
                ", fillColor = " + fillColor +
                ", borderColor = " + borderColor +
                " }";
    }

    @Override
    public int compareTo(Shape shape) {
        return this.fillColor.compareTo(shape.fillColor());
    }
}

class Triangle implements Shape, Comparable<Shape> {
    private final double ground;
    private final double height;
    private Color fillColor;
    private Color borderColor;

    Triangle(double ground, double height, Color fillColor, Color borderColor) {
        this.ground = ground;
        this.height = height;
        this.fillColor = fillColor;
        this.borderColor = borderColor;
        if (ground <= 0 || height <= 0) {
            throw new IllegalArgumentException("Parameters can't be <= 0");
        }
    }

    @Override
    public double area() {
        return (0.5 * (ground * height));
    }

    @Override
    public Color fillColor() {
        return fillColor;
    }

    @Override
    public Color borderColor() {
        return borderColor;
    }

    @Override
    public String toString() {
        return "Triangle" +
                " { " +
                "ground = " + ground +
                ", height = " + height +
                ", fillColor = " + fillColor +
                ", borderColor = " + borderColor +
                " }";
    }

    @Override
    public int compareTo(Shape shape) {
        return this.fillColor.compareTo(shape.fillColor());
    }
}