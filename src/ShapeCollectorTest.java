import org.junit.jupiter.api.Assertions;

class ShapeCollectorTest {
    private ShapeCollector<Shape> collector = new ShapeCollector<>();

    @org.junit.jupiter.api.Test
    void add() {
        collector.add(new Circle(5, Color.CORAL, Color.GREEN_SEA));
        Assertions.assertEquals(collector.printCountFigures(), 1);
    }

    @org.junit.jupiter.api.Test
    void findMinArea() {
        Shape circle = new Circle(10, Color.BEIGE, Color.GREEN_SEA);
        Shape circle1 = new Circle(5, Color.CORAL, Color.GREEN_SEA);
        Shape square = new Square(22, Color.CORAL, Color.GREEN_SEA);
        Shape rectangle = new Rectangle(10, 12, Color.CORAL, Color.GREEN_SEA);
        Shape triangle = new Triangle(5, 10, Color.WHITE, Color.TOMATO);
        collector.add(circle);
        collector.add(circle1);
        collector.add(square);
        collector.add(rectangle);
        collector.add(triangle);
        Assertions.assertEquals(collector.findMinArea(), triangle);
    }

    @org.junit.jupiter.api.Test
    void findMaxArea() {
        Shape circle = new Circle(10, Color.BEIGE, Color.GREEN_SEA);
        Shape circle1 = new Circle(5, Color.CORAL, Color.GREEN_SEA);
        Shape square = new Square(22, Color.CORAL, Color.GREEN_SEA);
        Shape rectangle = new Rectangle(10, 12, Color.CORAL, Color.GREEN_SEA);
        Shape triangle = new Triangle(5, 10, Color.WHITE, Color.TOMATO);
        collector.add(circle);
        collector.add(circle1);
        collector.add(square);
        collector.add(rectangle);
        collector.add(triangle);
        Assertions.assertEquals(collector.findMaxArea(), square);
    }

    @org.junit.jupiter.api.Test
    void allFiguresSquare() {
        Shape circle = new Circle(10, Color.BEIGE, Color.GREEN_SEA);
        Shape circle1 = new Circle(5, Color.CORAL, Color.GREEN_SEA);
        Shape square = new Square(22, Color.CORAL, Color.GREEN_SEA);
        Shape rectangle = new Rectangle(10, 12, Color.CORAL, Color.GREEN_SEA);
        Shape triangle = new Triangle(5, 10, Color.WHITE, Color.TOMATO);
        collector.add(circle);
        collector.add(circle1);
        collector.add(square);
        collector.add(rectangle);
        collector.add(triangle);
        Assertions.assertEquals(collector.allFiguresSquare(), 1021.6990816987242);
    }

    @org.junit.jupiter.api.Test
    void findByFillColor() {
        Shape circle = new Circle(10, Color.BEIGE, Color.GREEN_SEA);
        collector.add(circle);
        Assertions.assertEquals(collector.findByFillColor(Color.BEIGE), collector.getAllShapes());
    }

    @org.junit.jupiter.api.Test
    void findByBorderColor() {
        Shape circle = new Circle(10, Color.BEIGE, Color.TOMATO);
        collector.add(circle);
        Assertions.assertEquals(collector.findByBorderColor(Color.TOMATO), collector.getAllShapes());
    }

    @org.junit.jupiter.api.Test
    void printCountFigures() {
        Shape circle = new Circle(10, Color.BEIGE, Color.TOMATO);
        collector.add(circle);
        Assertions.assertEquals(collector.printCountFigures(), 1);
    }
}