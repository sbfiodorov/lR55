import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        ShapeCollector<Shape> collector = new ShapeCollector<>();

        Shape circle = new Circle(10, Color.BEIGE, Color.GREEN_SEA);
        Shape circle1 = new Circle(5, Color.CORAL, Color.GREEN_SEA);
        Shape square = new Square(22, Color.CORAL, Color.GREEN_SEA);
        Shape rectangle = new Rectangle(10, 12, Color.CORAL, Color.GREEN_SEA);
        Shape triangle = new Triangle(5, 10, Color.WHITE, Color.TOMATO);
        collector.add(circle);
        collector.add(circle1);
        collector.add(square);
        collector.add(rectangle);
        collector.add(triangle);

        Class<? extends Shape> aClass = circle.getClass();

        List<Shape> shapes = collector.figuresForType(aClass);
        List<Shape> shapes1 = Collections.emptyList();
        List<Square> squares = Collections.emptyList();

        collector.addAll(shapes1);
        List<Shape> v = collector.addAll(squares);
        System.out.println(v);

        for (Shape figure : shapes) {
            System.out.println();
            System.out.println("Площадь : " + figure.area());
            System.out.println("Цвет заливки : " + figure.fillColor());
            System.out.println("Цвет границы : " + figure.borderColor());
            System.out.println();
        }

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        System.out.println("Фигура с минимальной площадью, из всех имеющихся : " + collector.findMinArea());
        System.out.println("Фигура с максимальной площадью, из всех имеющихся : " + collector.findMaxArea());
        System.out.println("\nФигуры, которые имеются в списке : ");
        List<Shape> a = collector.getAllShapes();
        System.out.println(a);
        System.out.println();
        System.out.println("Кол-во имеющихся фигур : " + collector.printCountFigures());
        System.out.println("Суммарная площадь всех фигур : " + collector.allFiguresSquare() + "\n");
        System.out.println("Поиск всех фигур по цвету заливки : ");
        System.out.println(collector.findByFillColor(Color.CORAL));
        System.out.println();
        System.out.println("Поиск всех фигур по цвету границы : ");
        System.out.println(collector.findByBorderColor(Color.GREEN_SEA));
        System.out.println();
        System.out.println("\nФигуры, сгруппированные по цвету заливки : ");
        Map<Color, List<Shape>> map = collector.figuresForFillColor(a);
        System.out.println(map);
        System.out.println("\nФигуры, сгруппированные по цвету границы : ");
        Map<Color, List<Shape>> map2 = collector.figuresForBorderColor(a);
        System.out.println(map2);
        System.out.println("\nФигуры, сгруппированные по определённому типу : ");
        System.out.println(collector.figuresForType(aClass));
        System.out.println();
        List<Comparable> e = new ArrayList<>();
        e.add((Comparable) circle);
        e.add((Comparable) circle1);
        e.add((Comparable) square);
        e.add((Comparable) rectangle);
        e.add((Comparable) triangle);
        System.out.println(collector.getSorted(e));
    }
}