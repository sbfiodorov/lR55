public interface Shape {
    double area();

    Color fillColor();

    Color borderColor();

}