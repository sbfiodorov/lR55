import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ShapeCollector<T extends Shape> {
    private List<T> figures = new ArrayList<>();
    private List<T> list = new ArrayList<>();
    private Map<Color, List<T>> map = new HashMap<>();
    private Map<Color, List<T>> map2 = new HashMap<>();

    void add(T newShape) {
        figures.add(newShape);
    }

    T findMinArea() {
        return figures.stream().min(Comparator.comparing(Shape::area)).orElse(null);
    }

    T findMaxArea() {
        return figures.stream().max(Comparator.comparing(Shape::area)).orElse(null);
    }

    double allFiguresSquare() {
        double allArea = 0;
        for (T figure : figures) {
            allArea += figure.area();
        }
        return allArea;
    }

    List<T> findByFillColor(Color color) {
        Stream<T> stream = figures.stream();
        return stream.filter(s -> s.fillColor() == color).collect(Collectors.toList());
    }

    List<T> findByBorderColor(Color color) {
        Stream<T> stream = figures.stream();
        return stream.filter(s -> s.borderColor() == color).collect(Collectors.toList());
    }

    List<T> getAllShapes() {
        return new ArrayList<>(figures);
    }

    int printCountFigures() {
        return figures.size();
    }

    Map<Color, List<T>> figuresForFillColor(List<? extends T> figures) {
        for (T shape : figures) {
            if (map.containsKey(shape.fillColor())) {
                map.get(shape.fillColor()).add(shape);
            } else {
                map.put(shape.fillColor(), new ArrayList<>(Collections.singletonList(shape)));
            }
        }
        return map;
    }

    Map<Color, List<T>> figuresForBorderColor(List<? extends T> figures) {
        for (T shape : figures) {
            if (map2.containsKey(shape.borderColor())) {
                map2.get(shape.borderColor()).add(shape);
            } else {
                map2.put(shape.borderColor(), new ArrayList<>(Collections.singletonList(shape)));
            }
        }
        return map2;
    }

    List<T> figuresForType(Class<? extends T> aClass) {
        return figures.stream().filter(shape -> shape.getClass() == aClass).collect(Collectors.toList());
    }

    List<T> addAll(List<? extends T> lists) {
        list.addAll(lists);
        return list;
    }

    List<Comparable> getSorted(List<Comparable> a)
    {
        a.sort(Comparable::compareTo);
        return a;
    }
}
